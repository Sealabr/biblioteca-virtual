const router = require('express').Router();
const booksController = require('../controllers/books.controller');
const booksMiddleware = require('../middlewares/books.middlewares');

module.exports = (app) => {
  // New Book
  router.post('/', booksMiddleware.validate, booksController.create);

  // Get All books
  router.get('/', booksController.findAll);

  // get by id
  router.get('/:id', booksController.findOne);

  // Update by id
  router.put('/:id', booksController.update);

  // Delete by id
  router.delete('/:id', booksController.delete);

  app.use('/api/books', router);
};
