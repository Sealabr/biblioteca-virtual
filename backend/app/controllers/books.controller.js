const db = require('../models');

const Book = db.book;
const { Op } = db.Sequelize;

// Salvando um livro - POST
exports.create = async (req, res) => {
  if (typeof req.body !== 'object') req.body = JSON.parse(req.body);

  try {
    const exstingBook = await Book.findOne(
      { where: { title: req.body.title } }
    );

    if (exstingBook) {
      res.status(400).send({
        message: 'book already exists!',
      });
      return;
    }

    const book = {
      title: req.body.title,
      category: req.body.title,
      description: req.body.description,
      published_date: req.body.publishedAt,
      created_date: Date.now(),
      updated_date: Date.now(),
    };

    const data = await Book.create(book);

    res.send(data);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || 'Ops, there was an error creating a book',
    });
  }
};

// Busca todos os livros via parametro no get da variável ou q (realizar like nos campos)
exports.findAll = async (req, res) => {
  const { title } = req.query;
  let condition;

  try {
    if (req.query.q) {
      condition = {
        [Op.or]: [
          { title: { [Op.like]: `%${req.query.q}%` } },
          { description: { [Op.like]: `%${req.query.q}%` } },
          { category: { [Op.like]: `%${req.query.q}%` } },
        ],
      }
    } else {
      condition = title ? { title: { [Op.like]: `%${title}%` } } : null;
    }

    const data = await Book.findAll({ where: condition })

    res.send(data);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || 'Ops, there was an error fetching a book, please try again',
    });
  }
};

// buscar um único livro pelo id
exports.findOne = async (req, res) => {
  const { id } = req.params;

  try {
    const data = await Book.findByPk(id);

    if (!data) {
      res.status(404).send({
        message: `Book id: [${id}] was not found !`,
      });
    } else {
      res.send(data);
    }
  } catch (err) {
    res.status(500).send({
      message: `Ops, there was an error fetching a book with id: [${id}]`,
    });
  }
};

// atualiza o livro via put os dados no body
exports.update = async (req, res) => {
  if (typeof req.body !== 'object') req.body = JSON.parse(req.body);
  const { id } = req.params;

  try {
    req.body.updated_date = Date.now();

    const data = await Book.update(req.body, {
      where: { id },
    });

    if (data && data[0] === 1) {
      res.send({
        message: 'Book was updated successfully.',
      });
    } else {
      res.status(404).send({
        message: `Cant update Book with id: [${id}] or Book was not found!`,
      });
    }
  } catch (err) {
    res.status(500).send({
      message: `Error updating Book with id: [${id}]`,
    });
  }
};

// deleta um livro por um id
exports.delete = async (req, res) => {
  const { id } = req.params;

  try {
    const data = await Book.destroy({
      where: { id },
    })

    if (data === 1) {
      res.send({
        message: 'Book was deleted successfully.',
      });
    } else {
      res.status(404).send({
        message: `Cant deleted Book with id: [${id}] or Book was not found!`,
      });
    }
  } catch (err) {
    res.status(500).send({
      message: `Error deleting Book with id: [${id}]`,
    });
  }
};
