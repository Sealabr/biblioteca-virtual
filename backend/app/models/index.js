const Sequelize = require('sequelize');
const config = require('../database');

const sequelize = new Sequelize(config);

const db = {};

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.book = require('./book.model')(sequelize, Sequelize);

module.exports = db;
