module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define('book', {
    title: {
      type: DataTypes.STRING,
    },
    category: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.STRING,
    },
    published_date: {
      type: DataTypes.DATE,
    },
    created_date: {
      type: DataTypes.DATE,
    },
    updated_date: {
      type: DataTypes.DATE,
    },
  }, {
    timestamps: false,
  });

  return Book;
};
