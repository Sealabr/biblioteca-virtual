module.exports = {
  // eslint-disable-next-line arrow-body-style
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('books', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      title: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      category: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      description: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      published_date: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      created_date: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updated_date: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    });
  },

  // eslint-disable-next-line arrow-body-style
  down: (queryInterface) => {
    return queryInterface.dropTable('books');
  },
};
