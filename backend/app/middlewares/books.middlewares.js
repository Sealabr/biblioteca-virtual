const Yup = require('yup');

const validate = async (req, res, next) => {

  if (typeof req.body !== 'object') req.body = JSON.parse(req.body);

  const schema = Yup.object().shape({
    title: Yup.string()
      .min(1)
      .required(),
    category: Yup.string()
      .min(1)
      .required(),
    publishedAt: Yup.date()
      .required(),
  });

  try {
    const response = await schema.validate(req.body);

    next();
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
}

module.exports = {
  validate,
};
