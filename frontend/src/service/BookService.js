import axios from 'axios';

let API_BASE_URL;

// "react-scripts build" `NODE_ENV` is equal to `production`.
if (process.env.NODE_ENV === 'production') {
    API_BASE_URL = '/api/books';
// "react-scripts start" `NODE_ENV` is equal to `development`
}else{
    API_BASE_URL = 'http://localhost:8080/api/books';
}

class BookService {

    fetchBooks(search) {
        const queryString = search ? `?q=${search}` : "";
        return axios.get(API_BASE_URL + '/' + queryString);
    }

    fetchBookById(BookId) {
        return axios.get(API_BASE_URL + '/' + BookId);
    }

    deleteBook(BookId) {
        return axios.delete(API_BASE_URL + '/' + BookId);
    }

    AddBook(Book) {
        return axios.post(API_BASE_URL, Book);
    }

    editBook(Book) {
        return axios.put(API_BASE_URL + '/' + Book.id, Book);
    }

}

export default new BookService();