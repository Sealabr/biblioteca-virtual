import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
//import { useHistory } from "react-router-dom";

const style = {
    flexGrow: 1
}


const NavBar = () => {

    //const history = useHistory();
    //const navigateTo = () => history.push('/books');

    return (
        <div>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="Menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" style={style}>
                    <Button color="inherit">Biblioteca Virtual</Button>    
                    </Typography>
                    <Button color="inherit">Precisa de Ajuda?</Button>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default NavBar;
