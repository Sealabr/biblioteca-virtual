import React, { Component } from 'react'
import BookService from "../../service/BookService";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class AddBookComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            title: '',
            category: '',
            description: '',
            publishedAt: new Date()
        }
        this.saveBook = this.saveBook.bind(this);
        this.navigateIndex = this.navigateIndex.bind(this);
    }

    saveBook = (e) => {
        e.preventDefault();
        let book = {
            title: this.state.title, 
            category: this.state.category, 
            description: this.state.description, 
            publishedAt: this.state.publishedAt
        };
        BookService.AddBook(book)
            .then(res => {
                const message = 'Livro registrado com sucesso!';
                window.localStorage.setItem("message", message);
                this.props.history.push('/');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    navigateIndex() {
        window.localStorage.removeItem("bookId");
        this.props.history.push('/');
    }
    
    render() {
        return(
            <div>
                <Typography variant="h4" style={style}>Novo Livro</Typography>
                <form style={formContainer} onSubmit={this.saveBook}>

                    <TextField type="text" label="Título" placeholder="Informe o título do livro" fullWidth margin="normal" name="title" value={this.state.title} InputLabelProps={{ shrink: true, required: true }} onChange={this.onChange} required/>

                    <TextField type="text" label="Categoria" placeholder="Informe uma categoria para esse livro" fullWidth margin="normal" name="category" value={this.state.category} InputLabelProps={{ shrink: true, required: true }} onChange={this.onChange} required/>

                    <TextField name="publishedAt" label="Data de Publicação" type="date" margin="normal" onChange={this.onChange} fullWidth InputLabelProps={{ shrink: true, required: true }} />

                    <TextField multiline rows={2} rowsMax={4} label="Descrição" placeholder="Deixe uma descrição para esse livro" fullWidth margin="normal" name="description" value={this.state.description} onChange={this.onChange}/>

                    <Button type="submit" variant="contained" color="primary">Salvar</Button>

                    <Button style={{ marginLeft: 30 }} variant="contained" onClick={this.navigateIndex}>Voltar para Listagem</Button>
                    
                </form>
    </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style ={
    display: 'flex',
    justifyContent: 'center'

}

export default AddBookComponent;