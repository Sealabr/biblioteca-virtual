import React, { Component } from 'react'
import ApiService from "../../service/BookService";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { Alert } from '@material-ui/lab';

import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import * as moment from 'moment'

class ListBookComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            books: [],
            message: null,
            close: false,
            bookRef: null,
            open: false,
            filter: null,
            alertOpen: false
        }
        this.deleteBook = this.deleteBook.bind(this);
        this.editBook = this.editBook.bind(this);
        this.addBook = this.addBook.bind(this);
        this.reloadBooksList = this.reloadBooksList.bind(this);
        this.handleClose = this._handleClose.bind(this);
        this.handleOpen = this._handleOpen.bind(this);
        this.handleFilter = this._handleFilter.bind(this);
        this.handleAlertOpen = this._handleAlertOpen.bind(this);
        this.handleAlertClose = this._handleAlertClose.bind(this);

    }

    componentDidMount() {

        if(window.localStorage.getItem("message")){
            this.setState({ message: window.localStorage.getItem("message") });
            this.handleAlertOpen();
            window.localStorage.removeItem("message");
        }

        this.reloadBooksList();
    }

    reloadBooksList(search) {
        ApiService.fetchBooks(search)
            .then((res) => {
                console.log(res)
                this.setState({books: res.data})
            });
    }

    deleteBook(bookId) {
        ApiService.deleteBook(bookId)
           .then(res => {
               this.setState({message : 'Livro Excluído com sucesso!'});
               this.setState({books: this.state.books.filter(book => book.id !== bookId)});
           })
    }

    editBook(id) {
        window.localStorage.setItem("bookId", id);
        this.props.history.push('/edit-book');
    }

    addBook() {
        window.localStorage.removeItem("bookId");
        this.props.history.push('/add-book');
    }
    
    _handleFilter(filter) {
        this.setState({ filter: filter });
        this.reloadBooksList(filter);
    }

    _handleClose(confirm) {
        if(confirm){
            this.deleteBook(this.state.bookRef);
            this.handleAlertOpen();
        }
        this.setState({ open: false });
    }

    _handleOpen(bookRef) {
        this.setState({ open: true, bookRef: bookRef });
    }

    _handleAlertClose() {
        this.setState({ alertOpen: false });
    }

    _handleAlertOpen() {
        this.setState({ alertOpen: true });
    }
    
    render() {

        return (
            <div>

                <Collapse in={this.state.alertOpen} style={{ marginBottom: 10 }}>
                    <Alert
                    action={
                        <IconButton
                        aria-label="close"
                        color="inherit"
                        size="small"
                        onClick={() => {
                            this.handleAlertClose();
                        }}
                        >
                        <CloseIcon fontSize="inherit" />
                        </IconButton>
                    }
                    >
                    { this.state.message }
                    </Alert>
                </Collapse>

                <Typography variant="h4" style={title}>Listagem de Livros</Typography>

                <div style={spaceBetween}>
                <Button variant="contained" color="primary" onClick={() => this.addBook()}>
                    Cadastrar Livro
                </Button>
                <TextField style={filterField} onChange={e => this.handleFilter(e.target.value)} id="standard-basic" label="Pesquisar" placeholder="Título, Categoria ou descrição" />
                </div>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell style={tableTitle} align="left">Título</TableCell>
                            <TableCell style={tableTitle} align="left">Categoria</TableCell>
                            <TableCell style={tableTitle} align="left">Descrição</TableCell>
                            <TableCell style={tableTitle} align="left">Dt. publicação</TableCell>
                            <TableCell style={tableTitle} align="center" colSpan={2}>Ações</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.books.map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="left">{row.title}</TableCell>
                                <TableCell align="left">{row.category}</TableCell>
                                <TableCell align="left">{row.description}</TableCell>
                                <TableCell align="left">{moment(row.published_date).format('DD/MM/YYYY')}</TableCell>
                                <TableCell align="center" onClick={() => this.editBook(row.id)}><CreateIcon /></TableCell>
                                <TableCell align="center" onClick={() => this.handleOpen(row.id)} ><DeleteIcon /></TableCell>

                            </TableRow>
                        ))}
                    </TableBody>
                </Table>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Deseja Excluir o Livro?"}</DialogTitle>
                    <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Após a confirmação, essa ação não poderá ser desfeita, ok?
                    </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={()=>{ this.handleClose(false) }} color="primary">
                        Não
                    </Button>
                    <Button onClick={()=>{ this.handleClose(true) }} color="primary" autoFocus>
                        Sim, excluir!
                    </Button>
                    </DialogActions>
                </Dialog>

            </div>
        );
    }

}

const title ={
    display: 'flex',
    justifyContent: 'center'
}

const spaceBetween ={
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '20px',
    marginTop: '10px'
}

const tableTitle ={
    fontWeight: 'bold'
}

const filterField ={
    width: '40%'
}

export default ListBookComponent;