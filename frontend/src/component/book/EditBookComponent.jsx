import React, { Component } from 'react'
import BookService from "../../service/BookService";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import * as moment from 'moment'

class EditBookComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
            title: '',
            category: '',
            description: '',
            publishedAt: new Date()
        }
        this.saveBook = this.saveBook.bind(this);
        this.loadBook = this.loadBook.bind(this);
        this.navigateIndex = this.navigateIndex.bind(this);
    }

    componentDidMount() {
        this.loadBook();
    }

    loadBook() {
        BookService.fetchBookById(window.localStorage.getItem("bookId"))
            .then((res) => {
                let book = res.data;
                this.setState({
                id: book.id,
                title: book.title,
                category: book.category,
                description: book.description,
                publishedAt: moment(book.published_date).format('YYYY-MM-DD')
                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    saveBook = (e) => {
        e.preventDefault();
        let book = {
            id: this.state.id,
            title: this.state.title, 
            category: this.state.category, 
            description: this.state.description, 
            publishedAt: this.state.publishedAt
        };
        BookService.editBook(book)
            .then(res => {
                const message = 'As Informações do livro foram alteradas com sucesso';
                window.localStorage.setItem("message", message);
                this.props.history.push('/');
            });
    }

    navigateIndex() {
        window.localStorage.removeItem("bookId");
        this.props.history.push('/');
    }

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Editar Livro</Typography>
                <form style={formContainer} onSubmit={this.saveBook}>

                    <TextField type="text" label="Título" placeholder="Informe o título do livro" fullWidth margin="normal" name="title" value={this.state.title} onChange={this.onChange}/>

                    <TextField type="text" label="Categoria" placeholder="Informe uma categoria para esse livro" fullWidth margin="normal" name="category" value={this.state.category} onChange={this.onChange}/>

                    <TextField name="publishedAt" label="Data de Publicação" type="date" value={this.state.publishedAt} margin="normal" onChange={this.onChange} fullWidth InputLabelProps={{ shrink: true }} />

                    <TextField multiline rows={2} rowsMax={4} label="Descrição" placeholder="Deixe uma descrição para esse livro" fullWidth margin="normal" name="description" value={this.state.description} onChange={this.onChange}/>

                    <Button type="submit" variant="contained" color="primary">Alterar</Button>

                    <Button style={{ marginLeft: 30 }} variant="contained" onClick={this.navigateIndex}>Voltar para Listagem</Button>
                    
                </form>
            </div>
        );
    }
}

const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default EditBookComponent;