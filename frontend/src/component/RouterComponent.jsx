import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import ListBookComponent from "./book/ListBookComponent";
import AddBookComponent from "./book/AddBookComponent";
import EditBookComponent from "./book/EditBookComponent";
import React from "react";

const AppRouter = () => {
    return(
        <div style={style}>
            <Router>
                    <Switch>
                        <Route path="/" exact component={ListBookComponent} />
                        <Route path="/add-book" component={AddBookComponent} />
                        <Route path="/edit-book" component={EditBookComponent} />
                    </Switch>
            </Router>
        </div>
    )
}

const style={
    marginTop:'20px'
}

export default AppRouter;